stages:
  - quality
  - build
  - tests
  - deploy

workflow:
  rules:
    - changes:
      - docker/Dockerfile
      - node/**/*
      - pallets/**/*
      - runtime/**/*
      - .gitlab-ci.yml
      - Cargo.toml
      - Cargo.lock

.env:
  image: paritytech/ci-linux:production
  tags:
    - elois-neutron

fmt_and_clippy:
  extends: .env
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^wip*$/
      when: manual
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH == "master"'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - when: manual
  stage: quality
  script:
    - cargo fmt -- --version
    - cargo fmt -- --check
    - cargo clippy -- -V
    - cargo clippy --all --tests -- -D warnings

build_debug:
  extends: .env
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "master"'
      changes:
      - Cargo.lock
    - when: never
  stage: build
  script:
    - cargo clean -p duniter
    - cargo build --locked
    - mkdir build
    - mv target/debug/duniter build/duniter
  artifacts:
    paths:
      - build/
    expire_in: 3 day
  cache:
    - key:
        files:
          - Cargo.lock
      paths:
        - target/debug
      policy: push

build_debug_with_cache:
  extends: .env
  rules:
    - changes:
      - Cargo.lock
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH == "master"'
    - when: never
  stage: build
  script:
    - cargo clean -p duniter
    - cargo build --locked
    - mkdir build
    - mv target/debug/duniter build/duniter
  artifacts:
    paths:
      - build/
    expire_in: 3 day
  cache:
    - key:
        files:
          - Cargo.lock
      paths:
        - target/debug
      policy: pull

build_release:
  extends: .env
  rules:
    - if: "$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v*/"
    - when: never
  stage: build
  script:
    - cargo build --locked --release
    - mkdir build
    - mv target/release/duniter build/duniter
  artifacts:
    paths:
      - build/
    expire_in: 3 day

build_release_manual:
  extends: .env
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: manual
  stage: build
  allow_failure: true
  script:
    - cargo build --locked --release
    - mkdir build
    - mv target/release/duniter build/duniter
  artifacts:
    paths:
      - build/
    expire_in: 3 day

tests_debug:
  extends: .env
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^wip*$/
      when: manual
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_MERGE_REQUEST_ID || $CI_COMMIT_BRANCH == "master"'
    - when: manual
  stage: tests
  variables:
    DUNITER_BINARY_PATH: "../build/duniter"
    DUNITER_END2END_TESTS_SPAWN_NODE_TIMEOUT: "20"
  script:
    - cargo test --workspace --exclude duniter-end2end-tests
    - cargo cucumber -i balance*
    - cargo cucumber -i monetary*
    - cargo cucumber -i transfer*
  after_script:
    - cd target/debug/deps/
    - rm cucumber_tests-*.d
    - mv cucumber_tests* ../../../build/duniter-cucumber
  artifacts:
    paths:
      - build/
    expire_in: 3 day

tests_release:
  extends: .env
  rules:
    - if: "$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v*/"
    - when: never
  stage: tests
  variables:
    DUNITER_BINARY_PATH: "../build/duniter"
    DUNITER_END2END_TESTS_SPAWN_NODE_TIMEOUT: "20"
  script:
    - cargo test --workspace --exclude duniter-end2end-tests
    - cargo cucumber -i balance*
    - cargo cucumber -i monetary*
    - cargo cucumber -i transfer*
  after_script:
    - cd target/debug/deps/
    - rm cucumber_tests-*.d
    - mv cucumber_tests* ../../../build/duniter-cucumber
  artifacts:
    paths:
      - build/
    expire_in: 3 day
  dependencies:
    - build_release

.docker-build-app-image:
  stage: deploy
  image: docker:18.06
  tags:
    - redshift
  services:
    - docker:18.06-dind
  before_script:
    - docker info
  script:
    - docker pull $CI_REGISTRY_IMAGE:$IMAGE_TAG || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$IMAGE_TAG --pull -t "$CI_REGISTRY_IMAGE:$IMAGE_TAG" -f $DOCKERFILE_PATH .
    - docker login -u "duniterteam" -p "$DUNITERTEAM_PASSWD"
    - docker tag "$CI_REGISTRY_IMAGE:$IMAGE_TAG" "duniter/duniter-v2s:$IMAGE_TAG"
    - docker push "duniter/duniter-v2s:$IMAGE_TAG"

deploy_docker_test_image:
  extends: .docker-build-app-image
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^wip*$/
      when: manual
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH == "master"'
      when: never
    - when: manual
  allow_failure: true
  variables:
    DOCKERFILE_PATH: "docker/Dockerfile"
    IMAGE_TAG: "test-image-$CI_COMMIT_SHORT_SHA"

deploy_docker_debug_sha:
  extends: .docker-build-app-image
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
  variables:
    DOCKERFILE_PATH: "docker/Dockerfile"
    IMAGE_TAG: "debug-sha-$CI_COMMIT_SHORT_SHA"
  after_script:
    - docker login -u "duniterteam" -p "$DUNITERTEAM_PASSWD"
    - docker tag "duniter/duniter-v2s:$IMAGE_TAG" "duniter/duniter-v2s:debug-latest"
    - docker push "duniter/duniter-v2s:debug-latest"

deploy_docker_release_sha:
  extends: .docker-build-app-image
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: manual
  allow_failure: true
  variables:
    DOCKERFILE_PATH: "docker/Dockerfile"
    IMAGE_TAG: "sha-$CI_COMMIT_SHORT_SHA"
  dependencies:
    - build_release_manual

deploy_docker_release_tag:
  extends: .docker-build-app-image
  rules:
    - if: "$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v*/"
    - when: never
  variables:
    DOCKERFILE_PATH: "docker/Dockerfile"
    IMAGE_TAG: "$CI_COMMIT_TAG"
  after_script:
    - docker login -u "duniterteam" -p "$DUNITERTEAM_PASSWD"
    - docker tag "duniter/duniter-v2s:$IMAGE_TAG" "duniter/duniter-v2s:latest"
    - docker push "duniter/duniter-v2s:latest"
  dependencies:
    - build_release
